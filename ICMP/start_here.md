# DNS

**DNS Bruteforce:**

`gobuster dns -d domain.org -w /usr/share/wordlists/seclists/Discovery/DNS/subdomains-top1million-110000.txt -t 30`

**DNS Recon**

`dnsrecon –d yourdomain.com`

# Network

**Quick Scan:**

`nmap -vv -Pn -sT -sV -sC -oA 192.168.237.218"_quick" 192.168.237.218 --open`

**Full Scan:**

`nmap -vv -p- -Pn -sT -sV -sC -oA 192.168.237.218"_full" 192.168.237.218 --open`

# Web

**Directory Bruteforce:**

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-large-files.txt --hc 404 "http://192.168.237.218/FUZZ"`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt --hc 404 "http://192.168.237.218/FUZZ/"`

`feroxbuster -u http://192.168.237.218/ -t 10 -e -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt`

`gobuster dir -u http://192.168.237.218 -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-medium-directories.txt -k -t 30`

`gobuster dir -u http://192.168.237.218 -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-medium-files.txt -k -t 30`

`gobuster dir -u http://192.168.237.218 -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-medium-directories.txt -k -t 30`

`gobuster dir -u http://192.168.237.218 -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-medium-files.txt -k -t 30`

**Command Injection**

`wfuzz -c -z file,/usr/share/wordlists/seclists/Fuzzing/command-injection-commix.txt -d "doi=FUZZ" "http://192.168.237.218"`

**Local File Include**

`wfuzz -c -z file,/usr/share/wordlists/seclists/Fuzzing/LFI/LFI-Jhaddix.txt -d "doi=FUZZ" "http://192.168.237.218"`

**WPScan:**

***WPScan & SSL:***

`wpscan --url http://192.168.237.218/ --disable-tls-checks --enumerate p --enumerate t --enumerate u`

***WPScan Brute Forceing:***

`wpscan --url http://192.168.237.218/ --disable-tls-checks -U users -P /usr/share/wordlists/rockyou.txt`

***Aggressive Plugin Detection:***

`wpscan --url http://192.168.237.218/ --enumerate p --plugins-detection aggressive`

**Nikto:**

***Nikto with SSL and Evasion:***

`nikto --host $IP -ssl -evasion 1`

# SMTP

**SMTP User Enumeration:**

`smtp-user-enum -M VRFY -U /usr/share/wordlists/seclists/Usernames/xato-net-10-million-usernames.txt -t 192.168.237.218`

`smtp-user-enum -M EXPN -U /usr/share/wordlists/seclists/Usernames/xato-net-10-million-usernames.txt -t 192.168.237.218`

`smtp-user-enum -M RCPT -U /usr/share/wordlists/seclists/Usernames/xato-net-10-million-usernames.txt -t 192.168.237.218`

`smtp-user-enum -M EXPN -U /usr/share/wordlists/seclists/Usernames/xato-net-10-million-usernames.txt -t 192.168.237.218`

# BETTER SHELL:
```
python -c 'import pty; pty.spawn("/bin/bash")'
    OR
python3 -c 'import pty; pty.spawn("/bin/bash")'
    OR
/usr/bin/script -qc /bin/bash /dev/null

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/tmp ; export TERM=xterm-256color ;	alias ll='ls -lsaht --color=auto'
Ctrl + Z [Background Process]
stty raw -echo ; fg ; reset
stty columns 200 
```

# Misc

**Command Execution Verification - [Ping check]:**

`tcpdump -i any -c5 icmp`

# Resources

**Exploits**

https://www.exploit-db.com/

**Payloads**

https://www.revshells.com/

https://github.com/swisskyrepo/PayloadsAllTheThings

**HackTricks**

https://github.com/carlospolop/hacktricks/blob/master/README.md

https://book.hacktricks.xyz/welcome/readme

**PrivEsc**

https://gtfobins.github.io/

**Community**

https://sirensecurity.io/blog/

https://itm4n.github.io/
