### OS: 

### Web-Technology: 

### IP: `192.168.220.94`

### USERS: 

### CREDENTIALS (WEB): 

### CREDENTIALS (SYSTEM-LEVEL): 

### PROOF: 

---
**Quick Scan:**

`nmap -vv -Pn -sV -sC -oA 192.168.220.94"_quick" 192.168.220.94 --open`

**Full Scan:**

`nmap -vv -p- -Pn -sV -sC -oA 192.168.220.94"_full" 192.168.220.94 --open`

**Directory Bruteforce:**

`feroxbuster -u http://192.168.220.94/ -t 10 -e -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-files.txt --hc 404 "http://192.168.220.94/FUZZ"`

`wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/raft-medium-directories.txt --hc 404 "http://192.168.220.94/FUZZ/"`

---
NMAP RESULTS: 

```

```
---
ENUM:

```

```
---
BETTER SHELL:
```
python -c 'import pty; pty.spawn("/bin/bash")'
    OR
python3 -c 'import pty; pty.spawn("/bin/bash")'
    OR
/usr/bin/script -qc /bin/bash /dev/null

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/tmp ; export TERM=xterm-256color ;	alias ll='ls -lsaht --color=auto'
Ctrl + Z [Background Process]
stty raw -echo ; fg ; reset
stty columns 200 rows 200
```

---
STEPS:

1. 
2. 

---
Take Away Concepts:
